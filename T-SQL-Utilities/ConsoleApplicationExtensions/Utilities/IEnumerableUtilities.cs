﻿using System;
using System.Collections.Generic;

namespace ConsoleApplicationExtensions.Utilities
{
    public static class EnumerableUtilities
    {
        public static IEnumerable<T> Clone<T>(this IEnumerable<T> collection)
        {
            if (collection == null)
                throw new ArgumentNullException($"{nameof(collection)} cannot be null.");

            List<T> list = new List<T>();
            foreach (T t in collection)
            {
                list.Add(t);
            }

            return list;
        }
    }
}