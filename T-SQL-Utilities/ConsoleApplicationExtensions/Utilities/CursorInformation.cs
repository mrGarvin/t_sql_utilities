﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplicationExtensions.Utilities
{
    public class CursorInformation
    {
        private readonly IEnumerable<char> _charEnumerable;

        public int CursorStartColumn { get; }

        private int? ConstantCursorEndColumn { get; }
        public int CursorEndColumn
        {
            get
            {
                if (ConstantCursorEndColumn.HasValue)
                    return ConstantCursorEndColumn.Value;

                int columnCount = CursorStartColumn + _charEnumerable.Count();
                if (columnCount < Console.BufferWidth)
                    return columnCount;
                while (columnCount >= Console.BufferWidth)
                {
                    columnCount -= Console.BufferWidth;
                }
                return columnCount;
            }
        }

        public int CursorStartRow { get; }
        public int CursorEndRow
        {
            get
            {
                int columnCount = CursorStartColumn + _charEnumerable.Count(),
                cursorEndRow = CursorStartRow;
                while (columnCount >= Console.BufferWidth)
                {
                    cursorEndRow++;
                    columnCount -= Console.BufferWidth;
                }
                return cursorEndRow;
            }
        }
        
        public int LastCursorColumn
        {
            get { return _lastCursorColumn; }
            set
            {
                _lastCursorColumn = value;
                if (CursorStartRow == CursorEndRow && _lastCursorColumn < CursorStartColumn)
                    throw new ArgumentOutOfRangeException($"{nameof(_lastCursorColumn)} cannot be less than {nameof(CursorEndColumn)} when {nameof(Console.CursorTop)} are equal to {nameof(CursorStartRow)}.");
                if (_lastCursorColumn < 0)
                    throw new ArgumentOutOfRangeException($"{nameof(_lastCursorColumn)} cannot be less than zero.");
                if (_lastCursorColumn >= Console.BufferWidth)
                    throw new ArgumentOutOfRangeException($"{nameof(_lastCursorColumn)} cannot be larger than or equal to the buffer width of the Console.");
            }
        }
        private int _lastCursorColumn;

        public int LastCursorRow
        {
            get { return _lastCursorRow; }
            set
            {
                _lastCursorRow = value;
                if (_lastCursorRow < CursorStartRow)
                    throw new ArgumentOutOfRangeException($"{nameof(_lastCursorRow)} cannot be less than {nameof(CursorStartRow)}");
                if (_lastCursorRow > CursorEndRow)
                    throw new ArgumentOutOfRangeException($"{nameof(_lastCursorRow)} cannot be larger than {nameof(CursorEndRow)}");
            }
        }
        private int _lastCursorRow;
        
        public int ColumnIndex
        {
            get
            {
                if (CursorAtStartRow)
                    return Console.CursorLeft - CursorStartColumn;
                int columnIndex = Console.BufferWidth - CursorStartColumn;
                columnIndex += Console.CursorLeft;
                if (NumberOfRows == 2)
                    return columnIndex;
                columnIndex += Console.BufferWidth * (NumberOfRows - 2);
                return columnIndex;
            }
        }

        public int NumberOfRows => CursorEndRow - CursorStartRow + 1;
        public bool CursorAtStartRow => Console.CursorTop == CursorStartRow;
        public bool CursorAtEndRow => Console.CursorTop == CursorEndRow;
        public bool CursorAtStartColumn => CursorAtStartRow && Console.CursorLeft == CursorStartColumn;
        public bool CursorAtEndColumn => CursorAtEndRow && (Console.CursorLeft == CursorEndColumn || (ConstantCursorEndColumn != null && Console.CursorLeft == ConstantCursorEndColumn));
        public bool CanMoveCursorUp => !CursorAtStartRow;

        public CursorInformation(IEnumerable<char> charEnumerable, int cursorStartColumn, int cursorStartRow, int? constantCursorEndColumn = null)
        {
            ValidateInparamters(charEnumerable, cursorStartColumn, cursorStartRow, constantCursorEndColumn);
            _charEnumerable = charEnumerable;
            CursorStartColumn = cursorStartColumn;
            CursorStartRow = cursorStartRow;
            ConstantCursorEndColumn = constantCursorEndColumn;
            LastCursorColumn = CursorStartColumn;
            LastCursorRow = CursorStartRow;
        }

        private void ValidateInparamters(IEnumerable<char> charEnumerable, int cursorStartColumn, int cursorStartRow, int? constantCursorEndColumn = null)
        {
            if (charEnumerable is null)
                throw new ArgumentNullException($"{nameof(charEnumerable)} cannot be null.");

            if (cursorStartColumn < 0)
                throw new ArgumentOutOfRangeException($"{nameof(cursorStartColumn)} cannot be less than 0.");
            if (cursorStartColumn >= Console.BufferWidth)
                throw new ArgumentOutOfRangeException($"{nameof(cursorStartColumn)} cannot be lager than or equal to {nameof(Console.BufferWidth)}.");

            if (cursorStartRow < 0)
                throw new ArgumentOutOfRangeException($"{nameof(cursorStartRow)} cannot be less than 0.");
            if (cursorStartRow >= Console.BufferWidth)
                throw new ArgumentOutOfRangeException($"{nameof(cursorStartRow)} cannot be lager than or equal to {nameof(Console.BufferHeight)}.");

            if (constantCursorEndColumn < 0)
                throw new ArgumentOutOfRangeException($"{nameof(constantCursorEndColumn)} cannot be less than 0.");
            if (constantCursorEndColumn >= Console.BufferWidth)
                throw new ArgumentOutOfRangeException($"{nameof(constantCursorEndColumn)} cannot be lager than or equal to {nameof(Console.BufferWidth)}.");
        }

        public void SetLastCursorColumnAndRow(int lastCursorColumn, int lastCursorRow)
        {
            LastCursorColumn = lastCursorColumn;
            LastCursorRow = lastCursorRow;
        }

        public void UpdateLastCursorColumnAndRow()
        {
            LastCursorColumn = Console.CursorLeft;
            LastCursorRow = Console.CursorTop;
        }
    }
}