﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ConsoleApplicationExtensions;
using ConsoleApplicationExtensions.Utilities;

namespace T_SQL_Utilities
{
    internal class Program : ConsoleExtension
    {
        private struct WordLocation
        {
            public int StartPosition { get; }
            public int EndPosition { get; }

            public WordLocation(int startPosition, int endPosition)
            {
                if (startPosition < 0)
                    throw new ArgumentOutOfRangeException($"{nameof(startPosition)} cannot be less than 0.");
                if (endPosition < startPosition)
                    throw new ArgumentOutOfRangeException($"{nameof(endPosition)} cannot be less than {nameof(startPosition)}.");

                StartPosition = startPosition;
                EndPosition = endPosition;
            }
        }

        private static string ProgramName => nameof(T_SQL_Utilities).Replace("_", "-");
        private string ReservedKeywordsFileName => "T-SQLReservedKeywords.txt";
        private string ReservedKeywordsFilePath { get; set; }
        private bool _ignoreLinesStartingWithDoubleDashes = true;
        private IEnumerable<string> ReservedKeywords { get; set; }
        private string InputFilePath { get; set; }
        private Encoding InputFileEncoding { get; set; }
        
        static void Main(string[] args)
        {
            Console.Title = ProgramName;
            new Program();
        }

        protected override void Initialize()
        {
            SetProgramName(ProgramName);
            Console.SetBufferSize(120, 1000);
            Console.SetWindowSize(120, 30);
            
            AddCommand(ConsoleKey.A, "About", About);
            AddCommand(ConsoleKey.S, "Set '--' ignore", SetIgnoreLinesStartingWithDoubleDashes);
            AddCommand(ConsoleKey.U, "Uppercase reserved keywords", UppercaseReservedKeywords);
            AddCommand(ConsoleKey.I, "Insert brackets", InsertBracketsAroundColumnNames);
            AddCommand(ConsoleKey.C, "Clear file path", ClearInputFilePath);
            AddCommand(ConsoleKey.Q, "Quit", Quit);
        }

        private void About()
        {
            PrintMethodName(GetLabelOf(ConsoleKey.A));
            
            Dictionary<string, string[]> labelsAndDescriptions = new Dictionary<string, string[]>
            {
                { GetLabelOf(ConsoleKey.S), new []{ "Sets if lines that starts with '--' in the input file should be ignored." } },
                {
                    GetLabelOf(ConsoleKey.U), new[] { "Capitalizes all words in the input that match words that are found in the file:",
                                                     $"{ReservedKeywordsFileName}",
                                                      "Ignores words inside \'\' and []." }
                },
                { GetLabelOf(ConsoleKey.I), new[] { "Inserts brackets around all database, table, column, function, stored procedure",
                                                    "and trigger names in the input file." } },
                { GetLabelOf(ConsoleKey.C), new []{ "Clears the temporarily stored input file path." } },
                { ReservedKeywordsFileName, new[] { "Lines that start with '--' are ignored.",
                                                    "If a reserved keyword is missing, just add it to the file." }
                }
            };
            PrintAbout("Martin Lindstedt", labelsAndDescriptions);
        }

        private void SetIgnoreLinesStartingWithDoubleDashes()
        {
            PrintMethodName(GetLabelOf(ConsoleKey.I));

            bool? ignore = AskForBool("Ignore lines that starts with '--'?");
            if (ignore is null)
                return;
            _ignoreLinesStartingWithDoubleDashes = ignore.Value;
            PrintReturningToMainMenu($"Ignore lines starting with '--' set to: {_ignoreLinesStartingWithDoubleDashes}", null, false);
        }
        
        private void UppercaseReservedKeywords()
        {
            PrintMethodName(GetLabelOf(ConsoleKey.U));

            IList<string> fileContent;
            try
            {
                RunMethodSetup(out fileContent);
                if (fileContent is null)
                    return;
            }
            catch (Exception e)
            {
                PrintReturningToMainMenu("Method setup failed.", e);
                return;
            }

            List<IEnumerable<WordLocation>> affectedWordLocationsInFile = new List<IEnumerable<WordLocation>>();
            int wordCounter = 0;
            for (int i = 0; i < fileContent.Count; i++)
            {
                string line = fileContent[i];
                ReservedKeywordsToUppercase(ref line, ref wordCounter, out IEnumerable<WordLocation> affectedWordLocations);
                fileContent[i] = line;
                affectedWordLocationsInFile.Add(affectedWordLocations.Any() ? affectedWordLocations : null);
            }
            if (wordCounter == 0)
            {
                PrintReturningToMainMenu("No changes made.", null, false);
                return;
            }
            Print($"{wordCounter} words to uppercase.", false, ConsoleColor.DarkGreen);
            bool? viewResult = AskForBool("Do you want to view the result?");
            if (viewResult is null)
                return;
            if (viewResult.Value)
                PrintFileContent(fileContent, affectedWordLocationsInFile);

            string filePath = InputFilePath;
            SaveToFile(fileContent, ref filePath, "UPPERCASE");
            if (filePath is null)
                return;

            PrintReturningToMainMenu($"Saved to:\n{filePath}", null, false);
        }

        private void InsertBracketsAroundColumnNames()
        {
            PrintMethodName(GetLabelOf(ConsoleKey.I));
            
            IList<string> fileContent;
            try
            {
                RunMethodSetup(out fileContent);
                if (fileContent is null)
                    return;
            }
            catch (Exception e)
            {
                PrintReturningToMainMenu("Method setup failed.", e);
                return;
            }

            List<IEnumerable<WordLocation>> affectedWordLocationsInFile = new List<IEnumerable<WordLocation>>();
            int wordCounter = 0;
            for (int i = 0; i < fileContent.Count; i++)
            {
                string line = fileContent[i];
                InsertBrackets(ref line, ref wordCounter, out IEnumerable<WordLocation> affectedWordLocations);
                fileContent[i] = line;
                affectedWordLocationsInFile.Add(affectedWordLocations.Any() ? affectedWordLocations : null);
            }
            if (wordCounter == 0)
            {
                PrintReturningToMainMenu("No changes made.", null, false);
                return;
            }
            Print($"{wordCounter} words affected.", false, ConsoleColor.DarkGreen);
            bool? viewResult = AskForBool("Do you want to view the result?");
            if (viewResult is null)
                return;
            if (viewResult.Value)
                PrintFileContent(fileContent, affectedWordLocationsInFile);

            string filePath = InputFilePath;
            SaveToFile(fileContent, ref filePath, "WithBrackets");
            if (filePath is null)
                return;

            PrintReturningToMainMenu($"Saved to:\n{filePath}", null, false);
        }

        private void RunMethodSetup(out IList<string> inputFileContent)
        {
            inputFileContent = null;
            LoadReservedKeywords();
            if (ReservedKeywords is null)
                return;
            SetInputFilePath();
            if (InputFilePath is null)
                return;
            if (!File.Exists(InputFilePath))
            {
                PrintReturningToMainMenu("File not found.");
                InputFilePath = null;
                return;
            }
            inputFileContent = LoadInputFileContent(); ;
        }

        private void SetInputFilePath()
        {
            if (InputFilePath is null)
                InputFilePath = InputService.ReadLine("Drag the file into the console window or enter the file path");
        }

        private IList<string> LoadInputFileContent()
        {
            SetInputFileEncoding();
            if (InputFileEncoding is null)
                return null;
            try
            {
                return ReadFromFile(InputFilePath, InputFileEncoding);
            }
            catch (Exception e)
            {
                InputFilePath = null;
                InputFileEncoding = null;
                throw e;
            }
        }

        private void SetInputFileEncoding()
        {
            if (!(InputFileEncoding is null))
                return;
            InputFileEncoding = GetFileEncoding();
            if (InputFileEncoding is null)
                return;
            Print($"Loading the file with encoding: {InputFileEncoding.EncodingName}", false);
        }

        private void LoadReservedKeywords()
        {
            if (ReservedKeywordsFilePath is null)
            {
                try
                {
                    SetReservedKeywordsFilePath();
                }
                catch (Exception e)
                {
                    ReservedKeywords = null;
                    throw new Exception($"Could not find '{ReservedKeywordsFileName}'.", e);
                }
            }

            try
            {
                IList<string> fileContent = ReadFromFile(ReservedKeywordsFilePath, DetermineFileEncoding(ReservedKeywordsFilePath));
                for (int i = 0; i < fileContent.Count; i++)
                {
                    if (fileContent[i].StartsWith("--/*"))
                        fileContent.RemoveAt(i);
                }
                ReservedKeywords = fileContent;
            }
            catch (Exception e)
            {
                ReservedKeywords = null;
                throw new Exception($"Could not load '{ReservedKeywordsFileName}'.", e);
            }
        }

        private void SetReservedKeywordsFilePath()
        {
            if (TryGetPath.ToFile(ReservedKeywordsFileName, new[] {"Resources"}, false,
                out string path, out Exception exception))
                ReservedKeywordsFilePath = path;
            else
            {
                ReservedKeywordsFilePath = null;
                throw exception;
            }
        }

        /// <summary>
        /// Determines a text file's encoding by analyzing its byte order mark (BOM).
        /// Defaults to Encoding.Default when detection of the text file's endianness fails.
        /// </summary>
        /// <param name="filePath">The file to analyze.</param>
        /// <returns>The detected encoding.</returns>
        private Encoding DetermineFileEncoding(string filePath)
        {
            // Created by 2Toad
            // http://stackoverflow.com/questions/3825390/effective-way-to-find-any-files-encoding
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76)
                return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
                return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe)
                return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff)
                return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff)
                return Encoding.UTF32;
            return Encoding.Default;
        }

        private IList<string> ReadFromFile(string path, Encoding encoding)
        {
            if (path == null)
                throw new ArgumentNullException($"{nameof(path)} cannot be null.");
            if (encoding == null)
                throw new ArgumentNullException($"{nameof(encoding)} cannot be null.");

            List<string> lines = new List<string>();
            using (StreamReader sr = new StreamReader(path, encoding))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (line == null)
                        continue;

                    lines.Add(line);
                }
            }
            return lines;
        }

        private Encoding GetFileEncoding()
        {
            if (InputFilePath is null)
                throw new NullReferenceException(nameof(InputFilePath));
            bool? tryToDetermineFileEncoding = AskForBool("Try to determine the encoding of the file?");
            if (tryToDetermineFileEncoding is null)
                return null;
            Encoding fileEncoding;
            if (tryToDetermineFileEncoding.Value)
                fileEncoding = DetermineFileEncoding(InputFilePath);
            else
            {
                fileEncoding = GetFileEncodingFromInput();
                if (fileEncoding is null)
                    return null;
            }
            return fileEncoding;
        }

        private Encoding GetFileEncodingFromInput()
        {
            string[] options =
            {
                "UTF-7",
                "UTF-8",
                "UTF-16LE (Unicode)",
                "UTF-16BE (Big Endian Unicode)",
                "UTF-32",
                "ASCII",
                "Default (Operating system's current ANSI code page)"
            };

            Console.WriteLine();
            int? answer = AskForInt("Which encoding do you want to load the file with?", new OptionsStringArray(options),
                   new IntInputRange(1, options.Length));
            Encoding fileEncoding;
            switch (answer)
            {
                case 1:
                    fileEncoding = Encoding.UTF7;
                    break;
                case 2:
                    fileEncoding = Encoding.UTF8;
                    break;
                case 3:
                    fileEncoding = Encoding.Unicode;
                    break;
                case 4:
                    fileEncoding = Encoding.BigEndianUnicode;
                    break;
                case 5:
                    fileEncoding = Encoding.UTF32;
                    break;
                case 6:
                    fileEncoding = Encoding.ASCII;
                    break;
                case 7:
                    fileEncoding = Encoding.Default;
                    break;
                default:
                    fileEncoding = null;
                    break;
            }
            return fileEncoding;
        }

        private void PrintFileContent(IEnumerable<string> fileContent, IEnumerable<IEnumerable<WordLocation>> affectedWordLocationsInFile)
        {
            var linesArray = fileContent as string[] ?? fileContent.ToArray();
            for (int lineNumber = 0; lineNumber < linesArray.Count(); lineNumber++)
            {
                string line = linesArray[lineNumber];
                var affectedWordLocations = affectedWordLocationsInFile.ElementAt(lineNumber);
                if (affectedWordLocations is null)
                    Console.WriteLine(line);
                else
                {
                    var affectedWordLocationsInLine = affectedWordLocations as WordLocation[] ?? affectedWordLocations.ToArray();
                    int wordCounter = 0;
                    for (int charPosition = 0; charPosition < line.Length; charPosition++)
                    {
                        if (wordCounter < affectedWordLocationsInLine.Length)
                        {
                            if (charPosition == affectedWordLocationsInLine[wordCounter].StartPosition)
                                Console.BackgroundColor = ConsoleColor.DarkYellow;
                            else if (charPosition == affectedWordLocationsInLine[wordCounter].EndPosition + 1)
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                wordCounter++;
                            }
                        }
                        Console.Write(line[charPosition]);
                    }
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.WriteLine();
                }
            }
        }
        
        private void ReservedKeywordsToUppercase(ref string line, ref int wordCounter, out IEnumerable<WordLocation> affectedWords)
        {
            if (line is null)
                throw new ArgumentNullException(nameof(line));
            List<WordLocation> wordLocations = new List<WordLocation>();
            if (_ignoreLinesStartingWithDoubleDashes && line.StartsWith("--"))
            {
                affectedWords = wordLocations;
                return;
            }
            line += " ";
            char[] wordConnectors = { '_' };
            int startIndex = 0;
            while (true)
            {
                string word = GetNextWord(line, startIndex, wordConnectors, out int startIndexOfWord);
                if (word == null)
                    break;
                if (!word.IsUppercase() && ReservedKeywords.Contains(word.ToUpper()))
                {
                    line = line.UppercaseRange(startIndexOfWord, word.Length);
                    wordLocations.Add(new WordLocation(startIndexOfWord, startIndexOfWord + word.Length - 1));
                    wordCounter++;
                }
                startIndex = startIndexOfWord + word.Length;
                if (startIndex >= line.Length)
                    break;
            }
            affectedWords = wordLocations;
            line = line.Remove(line.Length - 1);
        }
        
        private void InsertBrackets(ref string line, ref int wordCounter, out IEnumerable<WordLocation> affectedWords)
        {
            if (line is null)
                throw new ArgumentNullException(nameof(line));
            List<WordLocation> wordLocations = new List<WordLocation>();
            if (_ignoreLinesStartingWithDoubleDashes && line.StartsWith("--"))
            {
                affectedWords = wordLocations;
                return;
            }
            line += " ";
            char[] wordConnectors = { '_' };
            int startIndex = 0;
            while (true)
            {
                string word = GetNextWord(line, startIndex, wordConnectors, out int startIndexOfWord);
                if (word == null)
                    break;
                if (ReservedKeywords.Contains(word.ToUpper()))
                    startIndex = startIndexOfWord + word.Length + 1;
                else
                {
                    InsertBracketsIn(ref line, startIndexOfWord, startIndexOfWord + word.Length);
                    wordLocations.Add(new WordLocation(startIndexOfWord, startIndexOfWord + word.Length + 1));
                    startIndex = startIndexOfWord + word.Length + 3;
                    wordCounter++;
                }
                
                if (startIndex >= line.Length)
                    break;
            }

            line = line.Remove(line.Length - 1);
            affectedWords = wordLocations;

            void InsertBracketsIn(ref string str, int startPosition, int endPosition)
            {
                str = str.Insert(endPosition, "]");
                str = str.Insert(startPosition, "[");
            }
        }

        private string GetNextWord(string str, int startIndex, char[] wordConnectors, out int startIndexOfWord)
        {
            if (string.IsNullOrEmpty(str))
            {
                startIndexOfWord = -1;
                return null;
            }
            if (startIndex < 0)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be less than 0.");
            if (startIndex >= str.Length)
                throw new ArgumentOutOfRangeException($"{nameof(startIndex)} cannot be larger or equal to the length of {nameof(str)}.");
            if (wordConnectors == null)
                throw new ArgumentNullException($"{nameof(wordConnectors)} cannot be null.");

            const char stringChar = '\'',
                       nameStartChar = '[',
                       nameEndChar = ']',
                       variableChar = '@';
            bool setStartIndex = true,
                 inString = false,
                 inName = false,
                 inVariable = false;
            for (int index = startIndex; index < str.Length; index++)
            {
                char character = str[index];
                if (character == stringChar)
                    inString = !inString;
                if (inString)
                    continue;

                if (character == nameStartChar || character == nameEndChar)
                    inName = !inName;
                if (inName)
                    continue;

                if (character == variableChar || (inVariable && (!char.IsLetter(character) || wordConnectors.Contains(character))))
                    inVariable = !inVariable;
                if (inVariable)
                    continue;

                if (setStartIndex && (char.IsLetter(character) || character == '_'))
                {
                    startIndex = index;
                    setStartIndex = false;
                    continue;
                }
                if (setStartIndex || char.IsLetter(character) || wordConnectors.Contains(character) || char.IsDigit(character))
                    continue;
                startIndexOfWord = startIndex;
                return str.Substring(startIndex, index - startIndex);
            }
            startIndexOfWord = -1;
            return null;
        }
        
        private void SaveToFile(IList<string> lines, ref string filePath, string fileNameExtension)
        {
            bool? overwriteOriginalFile = AskForBool("Do you want to overwrite the original file?", false);
            if (overwriteOriginalFile is null)
            {
                filePath = null;
                return;
            }
            filePath = overwriteOriginalFile.Value ? filePath : CreateNewFilePath(filePath, fileNameExtension);

            using (StreamWriter sw = new StreamWriter(filePath, false, Encoding.Unicode))
            {
                foreach (string line in lines)
                {
                    sw.WriteLine(line);
                }
            }
        }

        private string CreateNewFilePath(string filePath, string fileNameExtension)
        {
            if (filePath == null)
                throw new ArgumentNullException($"{nameof(filePath)}");

            string fileExtension = Path.GetExtension(filePath);
            filePath = filePath.Remove(filePath.Length - fileExtension.Length);
            uint fileCounter = 0;
            while (File.Exists(filePath + GetFileEnding(fileCounter, fileNameExtension, fileExtension)) && fileCounter < 10)
            {
                fileCounter++;
            }
            return filePath + GetFileEnding(fileCounter, fileNameExtension, fileExtension);
        }

        private string GetFileEnding(uint fileNumber, string fileNameAddition, string fileExtension)
        {
            if (fileNameAddition == null)
                throw new ArgumentNullException($"{nameof(fileNameAddition)}");
            if (fileExtension == null)
                throw new ArgumentNullException($"{nameof(fileExtension)}");
            StringBuilder outBuilder = new StringBuilder();
            outBuilder.Append("_");
            outBuilder.Append(fileNameAddition);
            if (fileNumber != 0)
            {
                outBuilder.Append("_");
                outBuilder.Append(fileNumber);
            }
            outBuilder.Append(fileExtension);

            return outBuilder.ToString();
        }

        private void ClearInputFilePath()
        {
            PrintMethodName(GetLabelOf(ConsoleKey.C));

            InputFilePath = null;
            InputFileEncoding = null;
            PrintReturningToMainMenu("File path cleared.", null, false);
        }

        private void Quit()
        {
            bool? answer = AskForBool("Do you really want to quit?");
            if (answer.HasValue && answer.Value)
                EndLoop();
        }
    }
}
