﻿using System;

namespace ConsoleApplicationExtensions.Utilities
{
    public static class ConsoleKeyUtilities
    {
        public static bool IsNumberKey(this ConsoleKey consoleKey)
        {
            return consoleKey.IsDigitKey() && consoleKey.IsNumPadKey();
        }

        public static bool IsDigitKey(this ConsoleKey consoleKey)
        {
            string str = consoleKey.ToString();
            return str.Length == 2 && str[0] == 'D' && char.IsDigit(str[1]);
        }

        public static bool IsNumPadKey(this ConsoleKey consoleKey)
        {
            string str = consoleKey.ToString();
            return str.Length == 7 && str.Contains("NumPad") && char.IsDigit(str[6]);
        }

        public static bool IsLetterKey(this ConsoleKey consoleKey)
        {
            switch (consoleKey)
            {
                case ConsoleKey.Oem6:   // Å
                    return true;
                case ConsoleKey.Oem7:   // Ä
                    return true;
                case ConsoleKey.Oem3:   // Ö
                    return true;
            }
            if (consoleKey.ToString().Length > 1)
                return false;
            return char.IsLetter(consoleKey.ToString(), 0);
        }

        public static ConsoleKey ToNumPadKey(this ConsoleKey consoleKey)
        {
            switch (consoleKey)
            {
                case ConsoleKey.D0:
                    return ConsoleKey.NumPad0;
                case ConsoleKey.D1:
                    return ConsoleKey.NumPad1;
                case ConsoleKey.D2:
                    return ConsoleKey.NumPad2;
                case ConsoleKey.D3:
                    return ConsoleKey.NumPad3;
                case ConsoleKey.D4:
                    return ConsoleKey.NumPad4;
                case ConsoleKey.D5:
                    return ConsoleKey.NumPad5;
                case ConsoleKey.D6:
                    return ConsoleKey.NumPad6;
                case ConsoleKey.D7:
                    return ConsoleKey.NumPad7;
                case ConsoleKey.D8:
                    return ConsoleKey.NumPad8;
                case ConsoleKey.D9:
                    return ConsoleKey.NumPad9;
                default:
                    return consoleKey;
            }
        }

        public static ConsoleKey ToDigitKey(this ConsoleKey consoleKey)
        {
            switch (consoleKey)
            {
                case ConsoleKey.NumPad0:
                    return ConsoleKey.D0;
                case ConsoleKey.NumPad1:
                    return ConsoleKey.D1;
                case ConsoleKey.NumPad2:
                    return ConsoleKey.D2;
                case ConsoleKey.NumPad3:
                    return ConsoleKey.D3;
                case ConsoleKey.NumPad4:
                    return ConsoleKey.D4;
                case ConsoleKey.NumPad5:
                    return ConsoleKey.D5;
                case ConsoleKey.NumPad6:
                    return ConsoleKey.D6;
                case ConsoleKey.NumPad7:
                    return ConsoleKey.D7;
                case ConsoleKey.NumPad8:
                    return ConsoleKey.D8;
                case ConsoleKey.NumPad9:
                    return ConsoleKey.D9;
                default:
                    return consoleKey;
            }
        }

        /// <summary>
        /// Converts the ConsoleKey to a letter if it is a letter.
        /// </summary>
        /// <param name="consoleKey">The ConsoleKey to convert.</param>
        /// <param name="toUpperCase">If true returns uppercase letter, else lowercase.</param>
        /// <returns>String letter if it is a letter key, else null.</returns>
        public static string ToLetter(this ConsoleKey consoleKey, bool toUpperCase = true)
        {
            switch (consoleKey)
            {
                case ConsoleKey.Oem6:
                    return toUpperCase ? "Å" : "å";
                case ConsoleKey.Oem7:
                    return toUpperCase ? "Ä" : "ä";
                case ConsoleKey.Oem3:
                    return toUpperCase ? "Ö" : "ö";
            }
            if (consoleKey.IsLetterKey())
                return toUpperCase ? consoleKey.ToString() : consoleKey.ToString().ToLower();

            return null;
        }

        /// <summary>
        /// Converts the ConsoleKey to an int if it is a number key.
        /// </summary>
        /// <param name="consoleKey"></param>
        /// <returns>Int if it is a number key, else -1.</returns>
        public static int ToInt(this ConsoleKey consoleKey)
        {
            switch (consoleKey)
            {
                case ConsoleKey.NumPad0:
                case ConsoleKey.D0:
                    return 0;
                case ConsoleKey.NumPad1:
                case ConsoleKey.D1:
                    return 1;
                case ConsoleKey.NumPad2:
                case ConsoleKey.D2:
                    return 2;
                case ConsoleKey.NumPad3:
                case ConsoleKey.D3:
                    return 3;
                case ConsoleKey.NumPad4:
                case ConsoleKey.D4:
                    return 4;
                case ConsoleKey.NumPad5:
                case ConsoleKey.D5:
                    return 5;
                case ConsoleKey.NumPad6:
                case ConsoleKey.D6:
                    return 6;
                case ConsoleKey.NumPad7:
                case ConsoleKey.D7:
                    return 7;
                case ConsoleKey.NumPad8:
                case ConsoleKey.D8:
                    return 8;
                case ConsoleKey.NumPad9:
                case ConsoleKey.D9:
                    return 9;
                default:
                    return -1;
            }
        }
    }
}