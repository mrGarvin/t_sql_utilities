﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplicationExtensions.Utilities
{
    public static class InputService
    {
        public enum DecimalSeparator
        {
            Dot,
            Comma
        }

        public static string[] ValidDateFormats { get; } = { "M/d/yyyy", "M/d/yy", "MM/dd/yy", "MM/dd/yyyy", "yy/MM/dd", "yyyy-MM-dd", "dd-MMM-yy" };

        public static string DefaultDateFormat => ValidDateFormats[5];

        private static CursorService CursorService { get; set; }

        public static string ReadLine(string prompt)
        {
            prompt += ": ";
            Console.Write(prompt);
            List<char> input = new List<char>();
            CursorService = new CursorService(new CursorInformation(input, prompt.Length, Console.CursorTop));

            while (true)
            {
                var consoleKeyInfo = Console.ReadKey(true);
                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        return null;
                    case ConsoleKey.Enter:
                        Console.WriteLine();
                        return InputToString(input);
                    case ConsoleKey.Backspace:
                        RemoveLeft(input);
                        break;
                    case ConsoleKey.Delete:
                        RemoveCurrent(input);
                        break;
                    default:
                        if (consoleKeyInfo.KeyChar != '\0')
                        {
                            int columnIndex = CursorService.CursorInformation.ColumnIndex;
                            if (columnIndex == input.Count)
                                input.Add(consoleKeyInfo.KeyChar);
                            else
                                input[columnIndex] = consoleKeyInfo.KeyChar;
                            Console.Write(consoleKeyInfo.KeyChar);
                            CursorService.CursorInformation.UpdateLastCursorColumnAndRow();
                        }
                        else
                            Move(consoleKeyInfo);
                        break;
                }
            }
        }

        public static DateTime? GetDate(string prompt, string format = "yyyy-MM-dd")
        {
            if (!ValidDateFormats.Contains(format))
                throw new ArgumentException($"'{format}' is not a valid format.");

            (int FirstPosition, int SecondPosition) GetDateSeparatorPositions()
            {
                int firstPos = (format.IndexOf("/", StringComparison.Ordinal) != -1) ? format.IndexOf("/", StringComparison.Ordinal) : format.IndexOf("-", StringComparison.Ordinal),
                    secondPos = (format.LastIndexOf("/", StringComparison.Ordinal) != -1) ? format.LastIndexOf("/", StringComparison.Ordinal) : format.LastIndexOf("-", StringComparison.Ordinal);
                return new ValueTuple<int, int>(firstPos, secondPos);
            }

            var separatorPositions = GetDateSeparatorPositions();

            prompt += ": ";
            int cursorStartColumn,
                cursorStartRow;
            int firstSeparatorPos = separatorPositions.FirstPosition,
                secondSeparatorPos = separatorPositions.SecondPosition;
            if (prompt.Length + format.Length >= Console.BufferWidth)
            {
                cursorStartColumn = 0;
                cursorStartRow = Console.CursorTop + 1;
                Console.WriteLine(prompt);
            }
            else
            {
                cursorStartColumn = prompt.Length;
                cursorStartRow = Console.CursorTop;
                Console.Write(prompt);
                firstSeparatorPos += cursorStartColumn;
                secondSeparatorPos += cursorStartColumn;
            }

            List<char> input = new List<char>();
            foreach (char c in format)
            {
                input.Add(c);
            }

            Console.Write(format);
            CursorService = new CursorService(new CursorInformation(input, cursorStartColumn, cursorStartRow));
            Console.SetCursorPosition(cursorStartColumn, cursorStartRow);

            while (true)
            {
                var consoleKeyInfo = Console.ReadKey(true);
                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        return null;
                    case ConsoleKey.Enter:
                        Console.WriteLine();
                        if (DateTime.TryParse(InputToString(input), out DateTime result))
                            return result;
                        break;
                    case ConsoleKey.LeftArrow:
                        MoveCursorHorizontally(CursorService.MoveCursorLeft);
                        break;
                    case ConsoleKey.RightArrow:
                        MoveCursorHorizontally(CursorService.MoveCursorRight);
                        break;
                    case ConsoleKey.Home:
                        CursorService.MoveCursorToHome();
                        break;
                    case ConsoleKey.End:
                        CursorService.MoveCursorToEnd();
                        break;
                    case ConsoleKey.Backspace:
                        if (CursorService.CursorInformation.CursorAtStartColumn)
                            break;
                        MoveCursorHorizontally(CursorService.MoveCursorLeft);
                        ReplaceAndReprint();
                        break;
                    case ConsoleKey.Delete:
                        if (CursorService.CursorInformation.ColumnIndex >= input.Count)
                            break;
                        ReplaceAndReprint();
                        break;
                    default:
                        if (char.IsDigit(consoleKeyInfo.KeyChar))
                        {
                            int columnIndex = CursorService.CursorInformation.ColumnIndex;
                            if (columnIndex >= input.Count)
                                break;

                            input[columnIndex] = consoleKeyInfo.KeyChar;
                            Console.Write(consoleKeyInfo.KeyChar);
                            if (Console.CursorLeft == firstSeparatorPos ||
                                Console.CursorLeft == secondSeparatorPos)
                                CursorService.MoveCursorRight();

                            CursorService.CursorInformation.UpdateLastCursorColumnAndRow();
                        }
                        break;
                }

                void MoveCursorHorizontally(Action moveAction)
                {
                    moveAction.Invoke();
                    if (Console.CursorLeft == firstSeparatorPos ||
                        Console.CursorLeft == secondSeparatorPos)
                        moveAction.Invoke();
                }

                void ReplaceAndReprint()
                {
                    input[CursorService.CursorInformation.ColumnIndex] = ' ';
                    Reprint(input);
                }
            }
        }

        public static uint? GetUInt(string prompt)
        {
            const uint maxUIntLength = 10;
            prompt += ": ";
            Console.Write(prompt);
            List<char> input = new List<char>();
            CursorInformation cursorInformation = new CursorInformation(input, prompt.Length, Console.CursorTop);
            CursorService = new CursorService(cursorInformation);

            while (true)
            {
                var consoleKeyInfo = Console.ReadKey(true);
                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        return null;
                    case ConsoleKey.Enter:
                        if (input.Count == 0)
                            break;
                        Console.WriteLine();
                        if (uint.TryParse(InputToString(input), out uint result))
                            return result;
                        return null;
                    case ConsoleKey.Backspace:
                        RemoveLeft(input);
                        break;
                    case ConsoleKey.Delete:
                        RemoveCurrent(input);
                        break;
                    default:
                        if (input.Count <= maxUIntLength && char.IsDigit(consoleKeyInfo.KeyChar) && cursorInformation.ColumnIndex < maxUIntLength)
                            AddToInput(consoleKeyInfo.KeyChar, input);
                        else
                            Move(consoleKeyInfo);
                        break;
                }
            }
        }

        public static int? GetInt(string prompt)
        {
            const int maxIntLength = 10;
            prompt += ": ";
            Console.Write(prompt);
            List<char> input = new List<char>();
            CursorInformation cursorInformation = new CursorInformation(input, prompt.Length, Console.CursorTop);
            CursorService = new CursorService(cursorInformation);

            while (true)
            {
                var consoleKeyInfo = Console.ReadKey(true);
                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        return null;
                    case ConsoleKey.Enter:
                        if (input.Count == 0)
                            break;
                        if (input.Count == 1 && input[0] == '-')
                            input.Add('0');
                        Console.WriteLine();
                        if (int.TryParse(InputToString(input), out int result))
                            return result;
                        return null;
                    case ConsoleKey.Backspace:
                        RemoveLeft(input);
                        break;
                    case ConsoleKey.Delete:
                        RemoveCurrent(input);
                        break;
                    default:
                        if (IsLessThanMaxIntLength() && IsValidCharacter() && CursorNotAtEndColumn())
                            AddToInput(consoleKeyInfo.KeyChar, input);
                        else
                            Move(consoleKeyInfo);
                        break;
                }

                bool IsLessThanMaxIntLength()
                {
                    var inputCount = input.Count;
                    if (input.Count > 0 && input[0] == '-')
                        inputCount--;
                    return inputCount <= maxIntLength;
                }

                bool IsValidCharacter()
                {
                    if (cursorInformation.ColumnIndex == 0 && consoleKeyInfo.KeyChar == '-')
                        return true;
                    return char.IsDigit(consoleKeyInfo.KeyChar);
                }

                bool CursorNotAtEndColumn()
                {
                    var endColumnIndex = maxIntLength;
                    if (input.Count > 0 && input[0] == '-')
                        endColumnIndex++;
                    return cursorInformation.ColumnIndex < endColumnIndex;
                }
            }
        }

        public static double? GetDouble(string prompt, DecimalSeparator decimalSeparator = DecimalSeparator.Dot)
        {
            prompt += ": ";
            Console.Write(prompt);
            char separator;
            switch (decimalSeparator)
            {
                case DecimalSeparator.Dot:
                    separator = '.';
                    break;
                case DecimalSeparator.Comma:
                    separator = ',';
                    break;
                default:
                    separator = '.';
                    break;
            }
            List<char> input = new List<char>();
            CursorInformation cursorInformation = new CursorInformation(input, prompt.Length, Console.CursorTop);
            CursorService = new CursorService(cursorInformation);

            while (true)
            {
                var consoleKeyInfo = Console.ReadKey(true);
                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        return null;
                    case ConsoleKey.Enter:
                        if (input.Count == 0)
                            break;
                        Console.WriteLine();
                        if (input.Count == 1 && input[0] == separator)
                            input.Insert(0, '0');
                        if (double.TryParse(InputToString(input), out double result))
                            return result;
                        return null;
                    case ConsoleKey.Backspace:
                        RemoveLeft(input);
                        break;
                    case ConsoleKey.Delete:
                        RemoveCurrent(input);
                        break;
                    default:
                        if (IsValidCharacter())
                            AddToInput(consoleKeyInfo.KeyChar, input);
                        else
                            Move(consoleKeyInfo);
                        break;
                }

                bool IsValidCharacter()
                {
                    if (consoleKeyInfo.KeyChar == '-' && cursorInformation.ColumnIndex == 0)
                        return true;
                    if (consoleKeyInfo.KeyChar == '.' && !input.Contains('.'))
                        return true;
                    return char.IsDigit(consoleKeyInfo.KeyChar);
                }
            }
        }

        public static bool? GetBool(string prompt, bool enterAsDefault = true)
        {
            Console.Write($"{prompt} {(enterAsDefault ? "(Y/n)" : "(y/N)")}: ");
            while (true)
            {
                var consoleKeyInfo = Console.ReadKey(true);
                switch (consoleKeyInfo.Key)
                {
                    case ConsoleKey.Escape:
                        Console.WriteLine();
                        return null;
                    case ConsoleKey.Enter:
                        Console.WriteLine(enterAsDefault ? 'Y' : 'N');
                        return enterAsDefault;
                    case ConsoleKey.Y:
                        Console.WriteLine(consoleKeyInfo.KeyChar);
                        return true;
                    case ConsoleKey.N:
                        Console.WriteLine(consoleKeyInfo.KeyChar);
                        return false;
                }
            }
        }

        private static void Move(ConsoleKeyInfo consoleKeyInfo)
        {
            switch (consoleKeyInfo.Key)
            {
                case ConsoleKey.LeftArrow:
                    CursorService.MoveCursorLeft();
                    break;
                case ConsoleKey.RightArrow:
                    CursorService.MoveCursorRight();
                    break;
                case ConsoleKey.UpArrow:
                    CursorService.MoveCursorUp();
                    break;
                case ConsoleKey.DownArrow:
                    CursorService.MoveCursorDown();
                    break;
                case ConsoleKey.Home:
                    CursorService.MoveCursorToHome();
                    break;
                case ConsoleKey.End:
                    CursorService.MoveCursorToEnd();
                    break;
            }
        }

        private static string InputToString(IEnumerable<char> input)
        {
            if (input == null)
                throw new ArgumentNullException($"{nameof(input)} cannot be null.");

            StringBuilder stringBuilder = new StringBuilder();
            foreach (char c in input)
            {
                stringBuilder.Append(c);
            }
            return stringBuilder.ToString();
        }

        private static void AddToInput(char character, IList<char> input)
        {
            int columnIndex = CursorService.CursorInformation.ColumnIndex;
            if (columnIndex == input.Count)
                input.Add(character);
            else
                input[columnIndex] = character;
            Console.Write(character);
            CursorService.CursorInformation.UpdateLastCursorColumnAndRow();
        }

        private static void RemoveLeft(IList input)
        {
            if (CursorService.CursorInformation.CursorAtStartColumn)
                return;
            CursorService.MoveCursorLeft();
            input.RemoveAt(CursorService.CursorInformation.ColumnIndex);
            Reprint(input);
        }

        private static void RemoveCurrent(IList input)
        {
            if (CursorService.CursorInformation.CursorAtEndColumn)
                return;
            input.RemoveAt(CursorService.CursorInformation.ColumnIndex);
            Reprint(input);
        }

        private static void Reprint(IList input)
        {
            int index = CursorService.CursorInformation.ColumnIndex;
            for (int i = index; i < input.Count; i++)
            {
                Console.Write(input[i]);
            }
            Console.Write("\0");
            CursorService.ResetCursorToLastPositon();
        }
    }
}
