﻿using System;

namespace ConsoleApplicationExtensions.Utilities
{
    public class CursorService
    {
        public CursorInformation CursorInformation { get; }

        public CursorService(CursorInformation cursorInformation)
        {
            CursorInformation = cursorInformation;
        }

        public void MoveCursorLeft()
        {
            if (CursorInformation.CursorAtStartColumn)
                return;
            if (Console.CursorLeft == 0)
            {
                Console.CursorLeft = Console.BufferWidth - 1;
                if (Console.CursorTop != 0)
                    Console.CursorTop--;
                CursorInformation.SetLastCursorColumnAndRow(Console.CursorLeft, Console.CursorTop);
            }
            else
            {
                Console.CursorLeft--;
                CursorInformation.LastCursorColumn = Console.CursorLeft;
            }
        }

        public void MoveCursorRight()
        {
            if (CursorInformation.CursorAtEndColumn)
                return;
            if (Console.CursorLeft == Console.BufferWidth - 1)
            {
                Console.CursorLeft = 0;
                Console.CursorTop++;
                CursorInformation.SetLastCursorColumnAndRow(Console.CursorLeft, Console.CursorTop);
            }
            else
            {
                Console.CursorLeft++;
                CursorInformation.LastCursorColumn = Console.CursorLeft;
            }
        }

        public void MoveCursorUp()
        {
            if (CursorInformation.CursorAtStartColumn)
                return;
            if (!CursorInformation.CursorAtStartRow)
            {
                Console.CursorTop--;
                CursorInformation.LastCursorRow = Console.CursorTop;
                if (CursorInformation.CursorAtStartRow &&
                Console.CursorLeft < CursorInformation.CursorStartColumn)
                    MoveCursorToHome();
            }
            else if (CursorInformation.CursorAtStartRow)
                MoveCursorToHome();
        }

        public void MoveCursorDown()
        {
            if (CursorInformation.CursorAtEndColumn)
                return;
            if (!CursorInformation.CursorAtEndRow)
            {
                Console.CursorTop++;
                CursorInformation.LastCursorRow = Console.CursorTop;
                if (CursorInformation.CursorAtEndRow &&
                Console.CursorLeft > CursorInformation.CursorEndColumn)
                    MoveCursorToEnd();
            }
            else if (CursorInformation.CursorAtEndRow)
                MoveCursorToEnd();
        }

        public void MoveCursorToHome()
        {
            Console.CursorLeft = CursorInformation.CursorAtStartRow ?
                                 CursorInformation.CursorStartColumn :
                                 0;
            CursorInformation.LastCursorColumn = Console.CursorLeft;
        }

        public void MoveCursorToEnd()
        {
            Console.CursorLeft = CursorInformation.CursorAtEndRow
                                 ? CursorInformation.CursorEndColumn
                                 : Console.BufferWidth - 1;
            CursorInformation.LastCursorColumn = Console.CursorLeft;
        }

        public void ResetCursorToLastPositon()
        {
            Console.CursorLeft = CursorInformation.LastCursorColumn;
            Console.CursorTop = CursorInformation.LastCursorRow;
        }
    }
}